#!/bin/bash
set -e
export BUN_INSTALL="/opt/build"
curl -fsSL https://bun.sh/install | bash
export PATH="$BUN_INSTALL/bun/bin:$PATH"
bun --version
