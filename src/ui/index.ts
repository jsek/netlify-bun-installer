import { NetlifyIntegrationUI } from "@netlify/sdk";

const integrationUI = new NetlifyIntegrationUI("ABC Integration");
const surface = integrationUI.addSurface("integrations-settings");
const route = surface.addRoute("/");

route.addCard(
  {
    title: "How it works",
    id: "how-it-works",
  },
  (card) => {
    card.addText({
      value: `
– Downloads Bun installation script and executes it

– Extends $PATH variable to enable \`bun\` and \`bunx\` commands

– Restores node dependencies (\`node_module\` folder) if \`bun.lockb\` file is found in base directory
    `,
    });
  }
);
route.addCard(
  {
    title: "Next steps",
    id: "next-steps",
  },
  (card) => {
    card.addText({
      value: `
– Disable NPM by setting environment variable

\`\`\`env
NPM_FLAGS = "--version"
\`\`\`

– Use \`bun\` or \`bunx\` commands to build your project
`,
    });
  }
);
route.addCard(
  {
    title: "Example",
    id: "example",
  },
  (card) => {
    card.addText({
      value: `
(NOTE: Please adapt commands for your own project.)

Example section in \`package.json\`

\`\`\`json
"scripts": {
  "prepare": "nuxi prepare",
  "build": "nuxi generate"
}
\`\`\`

Example \`netlify.toml\`

\`\`\`ini
[build]
  command = "bun run prepare && bun run build"
  publish = "dist"
\`\`\`
`,
    });
  }
);

export { integrationUI };
