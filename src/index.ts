import { NetlifyIntegration } from "@netlify/sdk";
import shell from "shelljs";
import fs from "fs";

const integration = new NetlifyIntegration();

integration.onEnable(async (_, { siteId, client }) => {
  siteId && (await client.enableBuildEventHandlers(siteId));
  return { statusCode: 200 };
});

integration.addBuildEventHandler("onPreBuild", ({ utils: { build } }) => {
  if (fs.existsSync("/opt/build") === false) {
    build.failBuild("[Bun Installer] Target installation directory '/opt/build' was not found");
    return;
  } else {
    try {
      shell.exec("test -w /opt/build");
    } catch (error) {
      build.failBuild("[Bun Installer] Target installation directory '/opt/build' exists but current process does not have 'write' permission", { error });
      return;
    }
  }

  try {
    shell.exec("./install-bun.sh");
  } catch (error) {
    build.failBuild("[Bun Installer] Bun installation failed", { error });
    return;
  }

  console.log("[Bun Installer] Installation successful");

  if (fs.existsSync("./bun.lockb") === false) {
    console.warn("[Bun Installer] 'bun.lockb' was not found in the current directory");
  } else {
    console.log("[Bun Installer] `bun.lockb` found, restoring dependencies...");

    try {
      shell.exec("bun i");
    } catch (error) {
      build.failBuild("[Bun Installer] Failed to restore dependencies", { error });
      return;
    }

    console.log("[Bun Installer] Dependencies were restored without errors");
  }
});

export { integration };
