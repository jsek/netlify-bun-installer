# Bun Installer for Netlify

Enable integration to speed up the build process.

![screenshot](https://gitlab.com/jsek/netlify-bun-installer/-/raw/master/screenshot.png)

## How it works

1. Downloads Bun installation script and executes it
1. Extends $PATH variable to enable `bun` and `bunx` commands
1. Restores node dependencies (`node_module` folder) if `bun.lockb` file is found in base directory

## Next steps

- Disable NPM by setting environment variable

```env
NPM_FLAGS = "--version"
```

- Use `bun` or `bunx` commands to build your project

> Following example works for Nuxt.js project. Please adapt commands for your own project.

Example section in `package.json`

```json
"scripts": {
  "prepare": "nuxi prepare",
  "build": "nuxi generate"
}
```

Example `netlify.toml`

```ini
[build]
  command = "bun run prepare && bun run build"
  publish = "dist"
```

Alternatively your build command might be straight `bunx nuxi prepare && bunx nuxi generate`.
